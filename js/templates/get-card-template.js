export const getCardTemplate = ({id, name, thumnail, description}) => (
    `<div class="card">
        <p>${name}</p>
        <img src="${thumnail}" alt="${name}" class="card-image"></img>
        <p>${description}</p>
        <a href="/${id}">${name}</a>
    </div>`
);
